const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");
var sex = "F";
var emailfol = "";

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});
app.use('/' , express.static('../front-end'))
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});
connection.connect(function (err) {
    console.log("Connected to database!");
    const sql = 
    "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),varsta VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,CNP VARCHAR(20),sex VARCHAR(20))";
    connection.query(sql, function (err, result) {
        if (err) 
        throw err;
    });
});
app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let varsta = req.body.varsta;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let CNP = req.body.CNP;
    let error = []

    if (!nume||!prenume||!telefon||!email||!nrCard||!facebook||!cvv||!tipAbonament||!varsta||!CNP) {
        error.push("Unul sau mai multe campuri nu au fost introduse!");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
      } else {
        if(nume.length<3||nume.length>30){
            console.log("Nume invalid!");
            error.push("Nume invalid");
        } else if(!nume.match("^[A-Za-z]+$")) {
            console.log("Numele trebuie sa contina doar litere!");
            error.push("Numele trebuie sa contina doar litere!");
             }
        if(prenume.length<3||prenume.length>30){
            console.log("Prenume invalid!");
            error.push("Prenume invalid");
        } else if(!prenume.match("^[A-Za-z]+$")) {
            console.log("Prenumele trebuie sa contica doar litere!");
            error.push("Prenumele trebuie sa ontina doar litere!");
             }
        if(telefon.length != 10) {
            console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
            error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
        } else if(!telefon.match("^[0-9]+$")) {
            console.log("Numarul de telefon trebuie sa contina doar cifre!");
            error.push("Numarul de telefon trebuie sa contina doar cifre!");
             }
        if(!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
            console.log("Email invalid!");
            error.push("Email invalid!");
        }
        if(!nrCard.length===16){
            console.log("Nr de card invalid!");
            error.push("Nr de card invalid!");
        } else if(!nrCard.match("^[0-9]+$")) {
            console.log("Nr de card trebuie sa contina numai cifre");
            error.push("Nr de card trebuie sa contina numai cifre");
             }
        if(!cvv===3){
            console.log("Cvv invalid!");
            error.push("Cvv invalid!");
        } else if(!nrCard.match("^[0-9]+$")) {
            console.log("Cvv trebuie sa contina numai cifre");
            error.push("Cvv trebuie sa contina numai cifre");
             }
        if(varsta.length<1||varsta.length>3){
            console.log("varsta invalid!");
            error.push("varsta invalid!");
        } else if(!varsta.match("^[0-9]+$")) {
            console.log("varsta trebuie sa contina numai cifre");
            error.push("varsta trebuie sa contina numai cifre");
             }
        if(!CNP.length===13){
            console.log("CNP invalid!");
            error.push("CNP invalid!");
        } else if(!CNP.match("^[0-9]+$")) {
            console.log("CNP trebuie sa contina numai cifre");
            error.push("CNP trebuie sa contina numai cifre");
             }
        }
        if(CNP[1]>="0"||CNP[1]<"1"){
            if(varsta!==(19-(parseInt(CNP[1])*10+parseInt(CNP[2]))).toString()) {
                console.log("Varsta si CNP nu se potrivesc!");
                error.push("Varsta si CNP nu se potrivesc!");
                } }
        else if(CNP[1]>"2")
            if(varsta!==(119-(parseInt(CNP[1])*10+parseInt(CNP[2]))).toString()){
                console.log("Varsta si CNP nu se potrivesc!");
                error.push("Varsta si CNP nu se potrivesc!");
                } 
        if(CNP[0]==="1"||CNP[0]==="3"||CNP[0]==="5")
                {
                    sex = "M";
                }
        else if(CNP[0]==="2"||CNP[0]==="4"||CNP[0]==="6")
                {
                    sex = "F";
                }
                else {
                        console.log("CNP invalid!");
                        error.push("CNP invalid!");
                     }

        if(emailfol.includes(email))
        {
            console.log("Email folosit!");
            error.push("Email folostit!");
        }
        else
        emailfol = emailfol + " " +email;

    if(error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,varsta,telefon,email,facebook,tipAbonament,nrCard,cvv,CNP,sex) VALUES('" +nume +"','" +prenume +"','"+varsta +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"','" +CNP +"','" +sex +"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});